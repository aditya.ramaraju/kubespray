# your Kubernetes cluster name here
cluster_name = "k8s-test-cluster"

# SSH key to use for access to nodes
public_key_path = "~/.ssh/lip_bob.pub"

# image to use for bastion, masters, standalone etcd instances, and nodes
image = "centos7-x86_64-raw"
# user on the node (ex. core on Container Linux, ubuntu on Ubuntu, etc.)
ssh_user = "centos"

# 0|1 bastion nodes
number_of_bastions = 1
flavor_bastion = "737f8483-8063-4567-a8e5-e09a4bcbdb49"

# standalone etcds
number_of_etcd = 0

# masters
number_of_k8s_masters = 0
number_of_k8s_masters_no_etcd = 0
number_of_k8s_masters_no_floating_ip = 1
number_of_k8s_masters_no_floating_ip_no_etcd = 0
flavor_k8s_master = "737f8483-8063-4567-a8e5-e09a4bcbdb49"
master_root_volume_size_in_gb = 50

# nodes
number_of_k8s_nodes = 0
number_of_k8s_nodes_no_floating_ip = 2
flavor_k8s_node = "737f8483-8063-4567-a8e5-e09a4bcbdb49"
node_root_volume_size_in_gb = 50

# GlusterFS
# either 0 or more than one
#number_of_gfs_nodes_no_floating_ip = 2
#gfs_volume_size_in_gb = 150
# Container Linux does not support GlusterFS
#image_gfs = "centos7-x86_64-raw"
# May be different from other nodes
#ssh_user_gfs = "centos"
#flavor_gfs_node = "737f8483-8063-4567-a8e5-e09a4bcbdb49"

# networking
network_name = "tiagonet2"
external_net = "49f17221-08fa-4c77-bee3-73c0b81bfde3"
#subnet_cidr = "<cidr>"
floatingip_pool = "public_net"
#bastion_allowed_remote_ips = ["0.0.0.0/0"]
master_allowed_ports = [{ "protocol" = "tcp", "port_range_min" = 22, "port_range_max" = 22, "remote_ip_prefix" = "0.0.0.0/0"}]
worker_allowed_ports = [{ "protocol" = "tcp", "port_range_min" = 22, "port_range_max" = 22, "remote_ip_prefix" = "0.0.0.0/0"}]
dns_nameservers = ["193.136.75.129", "193.136.75.130"]